;;; ===========================================================================
;;; RFC 1521 Section 7.2: The Multipart Content Type
;;; ===========================================================================
;;; Provides iteration over the parts of a multipart MIME message without
;;; reading all parts into memory.
;;;
;;;
;;; Terminology
;;; ===========
;;; encapsulation     The RFC refers to the distinct "parts" in a multipart
;;;                   message as an encapsulation.  This file typically call
;;;                   them mime-parts.
;;;
;;; delimiter         A character string that prefixes each mime-part.
;;;
;;; close-delimiter   The final delimiter in a multipart body.
;;;
;;; boundary          The string of characters created by the sending MIME
;;;                   software that is used to generate the delimiters.  This
;;;                   is a property of the Content-type header value.
;;;
;;; preamble          Text before the first delimiter.
;;;
;;; epilogue          Text after the close-delimiter.
;;;
;;;
;;;
;;; Copyright (C) 2013, Bryan Vicknair
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are met:
;;; 
;;; Redistributions of source code must retain the above copyright notice, this
;;; list of conditions and the following disclaimer.  Redistributions in binary
;;; form must reproduce the above copyright notice, this list of conditions and
;;; the following disclaimer in the documentation and/or other materials
;;; provided with the distribution.  Neither the name of the author nor the
;;; names of its contributors may be used to endorse or promote products
;;; derived from this software without specific prior written permission. 
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.


; TODO: Examples for each function in the function comments.
; TODO: Use len-delimited in high-level iteration functions?  Require
; content-length from user?

(module multipart
  (lfold-mime-parts map-mime-parts for-each-mime-part
   make-mime-part-iterator
   make-len-delimited-input-port
   make-multipart-body-input-port
   make-str-delimited-input-port
   make-str-peeker)



(import scheme chicken)
(use data-structures ports extras srfi-1 srfi-13)

(define version 0)
(define release 1)



;;; High-level mime-part iteration functions.
;;;
;;; Each iteration will receive a port that yields characters from the current
;;; mime-part.
;;;
;;; Note: foldr is impossible in this context without temporarily storing the
;;; entire message somewhere, which is something this module tries to avoid.
;;; We can't recurse into the other mime-parts, and then build up a solution
;;; once the recusion bottoms out as in the traditional fold.  Once we move to
;;; the next mime-part, the current mime-part's port is read to #!eof, so the
;;; data is lost by the time the recursion bottoms out.  Thus, only left fold
;;; is provided.

(define (lfold-mime-parts kons knil port boundary)
  (let ((next! (make-mime-part-iterator port boundary)))
    (let loop ((accum knil)
               (kar (next!)))
      (if (not kar)
        accum
        (loop (kons accum kar) (next!))))))

(define (map-mime-parts proc port boundary)
  (let ((kons (lambda (accum part) (cons (proc part) accum))))
    (reverse (lfold-mime-parts kons '() port boundary))))

(define (for-each-mime-part proc port boundary)
  (let ((kons (lambda (accum part) (proc part))))
    (lfold-mime-parts kons '() port boundary)))



;;; Creates a function that will return a port of the next mime-part each time
;;; it is called, or #f if no more mime-parts remain in the underlying port.
;;;
;;; port should be an input port that is currently at the top of a multipart
;;; MIME message body.
(define (make-mime-part-iterator port boundary)
  (let ((port (make-multipart-body-input-port port boundary))
       (prev-port #f))
    (lambda ()
      (when (and prev-port (char-ready? prev-port))
        ; TODO: Is there a way to read to #!eof of port without creating a
        ; string that will be GC'd?
        ; Finish reading the previous mime-port so the underlying port is at
        ; the boundary of the next mime-part.
        (read-string #f prev-port))
      (if (not (char-ready? port))
        #f
        (let ((part-port (make-str-delimited-input-port port (make-delim boundary))))
          (set! prev-port part-port)
          part-port)))))



;;; Note that the spec says that a delimiter includes of the \r\n ending the
;;; previous line, allowing mime-part bodies that don't end in a newline.
;;; Thus, a mime-part body ending in a newline, will have a visible blank line
;;; between the last line of the body and the boundary line when looking at the
;;; mime message.  Also note that delimiters must end in \r\n.
(define (make-delim boundary) (conc #\return #\newline
                                    "--" boundary
                                    #\return #\newline))
(define (make-close-delim boundary) (conc #\return #\newline
                                          "--" boundary "--"
                                          #\return #\newline))


;;; Make a custom port that reads the body of a MIME message body with a
;;; content type of multipart as specified in section 7.2 of RFC 1521.
;;;
;;; A multipart message body may start with a preamble, which should be ignored
;;; by MIME software.  After a possible preamble comes the first encapsulation
;;; delimiter.  A sequence of mime-parts follows.  A close-delimiter ends the
;;; sequence of mime-parts.  An epilogue may proceed the close-delimiter, but
;;; MIME software should ignore it.
;;;
;;; A port created with this function produces characters between the first
;;; delimiter and the close-delimiter, non-inclusive, or #!eof if a delimiter
;;; is never found in the underlying port.
;;;
;;; When reading a MIME message from a remote host, the port given to this
;;; function should normally be a port created with make-delimited-input-port
;;; with the 'len' argument set to the value of the content-length header, so
;;; that we don't try to read more than is present in the HTTP message body,
;;; which can result in an IO hang.
(define (make-multipart-body-input-port port boundary)
  (let* ((in-preamble? #t)
         (parts-port #f))

    (define (read-past-preamble)
      ; Read past the first delim.
      ; Though delimiters are specified to start with the \r\n of the
      ; proceeding line, we relax that specification here and only look for
      ; --<boundary>\r\n.  The reason for this is because clients of this
      ; function will probably have consumed the \r\n that separates the MIME
      ; headers from the body, thus, in the case of a multipart body with no
      ; preamble, the first character that we will see is the "-" of a
      ; delimiter, *not* the proper \r\n of a delimiter.
      (let ((preamble-port (make-str-delimited-input-port
                             port (string-drop (make-delim boundary) 2))))
        (read-string #f preamble-port))
      (set! parts-port (make-str-delimited-input-port port (make-close-delim boundary)))
      (set! in-preamble? #f))

    (define (read_)
      (cond (in-preamble? (read-past-preamble)(read_))
            (else (read-char parts-port))))

    (define (ready?)
      (when in-preamble?
        (read-past-preamble))
      (char-ready? parts-port))
    (define (close)
      (close-input-port port))
    (make-input-port read_ ready? close)))






;;; ===========================================================================
;;; Utilities
;;; ===========================================================================

;;; From http-client.  Port yields characters until EOF or len is reached.
;;;
;;; Use a port from this function with 'len' set to a MIME message's
;;; content-length for the input port to the multipart parsing functions so
;;; that they don't try to read more bytes than the remote host is willing to
;;; give us.
(define (make-len-delimited-input-port port len)
  (if (not len)
      port ;; no need to delimit anything
      (let ((pos 0))
        (make-input-port (lambda () ; read
                           (if (= pos len)
                               #!eof
                               (let ((char (read-char port)))
                                 (set! pos (add1 pos))
                                 char)))
                         (lambda () ; char-ready?
                           (if (= pos len)
                               #f
                               (char-ready? port)))
                         (lambda () ; close
                           (close-input-port port))))))


;;; Makes an "object" that behaves like a port, but provides a peek-str
;;; function.  After a call to peek-str, read-char will return the first
;;; character of the string previously returned by peek-str.  This can be
;;; useful when you need to "look-ahead" in a port further than one char.
;;;
;;; Note that interleaving calls to a str-peeker object and the standard port
;;; functions on the same underlying port will cause strange results because of
;;; the buffer used in this object.  After a (peek-str len) call, the character
;;; returned by (read-char) on the underlying port will be 'len' positions away
;;; from the character returned by a call to 'read-char from a peeker object.
(define (make-str-peeker port)
  (let* ((buf (make-queue))
         (buf-len 0)  ; queue-length not added until 4.8.
         (buf-add! (lambda (c) (queue-add! buf c)(set! buf-len (add1 buf-len))))
         (fill-buf (lambda (count)
                     (let* ((len (if (>= buf-len count) 0 (- count buf-len)))
                            (new-chars (string->list (read-string len port))))
                       (for-each buf-add! new-chars))))
         (buf-peek-char (lambda () (if (not (queue-empty? buf))
                                     (queue-first buf)
                                     (peek-char port))))
         (buf-read-char (lambda ()
                          (let ((char (if (not (queue-empty? buf))
                                       (begin (set! buf-len (sub1 buf-len))
                                              (queue-remove! buf))
                                       (read-char port))))
                            (set! prev-char char)
                            char)))
         (buf->string (lambda () (list->string (queue->list buf))))
         (buf-char-ready? (lambda () (not (eof-object? (buf-peek-char)))))
         (buf-close (lambda () (close-input-port port)))
         )

    (define (peek-str len)
      (fill-buf len)
      (string-take (buf->string) (min buf-len len)))

    ; Dispatch
    (lambda (method . args)
      (case method
        ((peek-char)    (buf-peek-char))
        ((read-char)    (buf-read-char))
        ((peek-str)     (apply peek-str args))
        ((char-ready?)  (buf-char-ready?))
        ((close-port)    (buf-close))
        (else (error `(str-peeker-unknown-message ,method)))))))



;;; Creates a port that reaches #!eof when target-str is found.
;;;
;;; Due to buffering, when a port created with this function returns #!eof
;;; because the target has been found, the underlying port will return the char
;;; after the found target-str on the next call to read-line.
(define (make-str-delimited-input-port port target-str)
  (if (string-null? target-str)
    (make-input-port (lambda()#!eof) (lambda()#f) (lambda()(close-input-port port)))
    (let ((buf (make-str-peeker port))
          (target-len (string-length target-str))
          (first-char-to-match (string-ref target-str 0))
          (target-found? #f))
      (define (read_)
        (if target-found?
          #!eof
          (let ((next (buf 'peek-char)))
            (cond ((eof-object? next) #!eof)
                  ((and (char=? next first-char-to-match)
                        (string=? target-str (buf 'peek-str target-len)))
                   (set! target-found? #t)
                   #!eof)
                  (else (buf 'read-char))))))
      (define (ready?)
        (and (not target-found?)
             (not (string=? target-str (buf 'peek-str target-len)))
             (buf 'char-ready?)))
      (define (close)
        (buf 'close-port))
      (make-input-port read_ ready? close))))

) ; end module
