SAL_DIR = /tmp/multipart-salmonella/


default: module


module: multipart.scm
	csc -s -O3 -j multipart multipart.scm


test: module tests/run.scm
	chicken-install -test -sudo

salmonella:
	rm -rf $(SAL_DIR)
	mkdir -p $(SAL_DIR)
	salmonella --log-file=$(SAL_DIR)log
	salmonella-html-report $(SAL_DIR)log $(SAL_DIR)html
	firefox $(SAL_DIR)html/index.html

clean:
	rm -f *.o *.so multipart.import.scm
	rm -f tests/multipart.so tests/multipart.import.scm tests/run
