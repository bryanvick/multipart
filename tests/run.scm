(use test multipart)



; Convenience test functions.
(define wifs with-input-from-string)
(define curin current-input-port)
(define (with-peeker str func)
  (wifs str (lambda ()
    (let ((peeker (make-str-peeker (curin))))
      (func peeker)))))
(define  (with-str-delimited-port port-content target-str func)
  (wifs port-content (lambda ()
    (let ((port (make-str-delimited-input-port (curin) target-str)))
      (with-input-from-port port func)))))
(define (with-multipart-port body boundary func)
  (wifs body (lambda ()
    (let ((port (make-multipart-body-input-port (curin) boundary)))
      (with-input-from-port port func)))))






(test-group "str-peeker"
  (with-peeker "foobar" (lambda (buf)
    (test "peek-char" #\f (buf 'peek-char))
    (test "read-char" #\f (buf 'read-char))
    (test "peek-str" "ooba" (buf 'peek-str 4))
    (test "peek-char-after-peek-str" #\o (buf 'peek-char))
    (test "read-char-after-peek-str" #\o (buf 'read-char))
    (test "peek-str eof" "obar" (buf 'peek-str 4)))))
(test-group "str-peeker buffer growing/shrinking"
  (with-peeker "foobarbaz" (lambda (buf)
    (test "peek-str part" "foobar" (buf 'peek-str 6))
    (test "peek-str all" "foobarbaz" (buf 'peek-str 9))
    (test "peek-str beyond" "foobarbaz" (buf 'peek-str 999))
    (test "peek-char works still" #\f (buf 'peek-char))
    (test "read-char works still" #\f (buf 'read-char)))))



(test-group "str-delimited-input-port"
  (with-str-delimited-port "foo\nbar" "" (lambda()
    (test "empty str target always found" "" (read-string))))
  (with-str-delimited-port "foo\nbar" "\nbar" (lambda()
    (test "stops @ str" "foo" (read-string))))
  (with-str-delimited-port "foo\nbar\nspam\n" "bar" (lambda()
    (test "stops @ target" "foo\n" (read-string))))
  (with-str-delimited-port "spam\nfoo\nfoobar" "foobar" (lambda()
    (test "handles lines smaller than target" "spam\nfoo\n" (read-string))))
  (with-str-delimited-port "barblabla\nfoobar\n" "bar" (lambda()
    (test "handles first line case" #!eof (read-char))))
  (with-str-delimited-port "foo\n\nbar" "" (lambda()
    (test "the empty string target is found immediately" "" (read-string))))
  (with-str-delimited-port "This is\r\nthe preamble\r\n--boundary\r\npart1" "--boundary" (lambda()
    (test "finds multipart delim" "This is\r\nthe preamble\r\n" (read-string))))
  (with-str-delimited-port "mime\nparts\r\n--boundary--\r\nepilogue" "--boundary--" (lambda()
    (test "finds multipart close-delim" "mime\nparts\r\n" (read-string))))
  (with-str-delimited-port "foo\nbar\nspam" "bar" (lambda()
    (test #t (char-ready?))
    (read-line)
    (test #f (char-ready?))  ; Should look ahead and see that target will be found.
    (test #f (char-ready?))  ; Multiple calls should not change the state.
    (test #f (char-ready?))
    (test #f (char-ready?))
  
    (test #!eof (read-char))
    (test #f (char-ready?))
    (test #f (char-ready?))
    (read-line)
    (test #f (char-ready?))))

  (wifs "foo\nbarHERE" (lambda ()
    (let ((port (make-str-delimited-input-port (curin) "bar")))
      (read-string #f port)
      (test "target found" #!eof (read-char port))
      (test "unerlying port @ char after target if found"
            #\H
            (read-char (curin)))
      ))))






;;; Simple example from RFC 1521 sec 7.2.1 page 31.
(define simple-body (string-translate* #<<END
This is the preamble.  It is to be ignored, though it
is a handy place for mail composers to include an
explanatory note to non-MIME conformant readers.
--simple boundary

This is implicitly typed plain ASCII text.
It does NOT end with a linebreak.
--simple boundary
Content-type: text/plain; charset=us-ascii

This is explicitly typed plain ASCII text.
It DOES end with a linebreak.

--simple boundary--
This is the epilogue.  It is also to be ignored.
END
'(("\n" . "\r\n")))
)

;;; Complex example from RFC 1521 Appendix C.
(define complex-body (string-translate* #<<END
MIME-Version: 1.0
From: Nathaniel Borenstein <nsb@bellcore.com>
To: Ned Freed <ned@innosoft.com>
Subject: A multipart example
Content-Type: multipart/mixed;
     boundary=unique-boundary-1

This is the preamble area of a multipart message.
Mail readers that understand multipart format
should ignore this preamble.
If you are reading this text, you might want to
consider changing to a mail reader that understands
how to properly display multipart messages.
--unique-boundary-1

   ...Some text appears here...
[Note that the preceding blank line means
no header fields were given and this is text,
with charset US ASCII.  It could have been
done with explicit typing as in the next part.]

--unique-boundary-1
Content-type: text/plain; charset=US-ASCII

This could have been part of the previous part,
but illustrates explicit versus implicit
typing of body parts.

--unique-boundary-1
Content-Type: multipart/parallel;
     boundary=unique-boundary-2


--unique-boundary-2
Content-Type: audio/basic
Content-Transfer-Encoding: base64

   ... base64-encoded 8000 Hz single-channel
             mu-law-format audio data goes here....
--unique-boundary-2
Content-Type: image/gif
Content-Transfer-Encoding: base64

   ... base64-encoded image data goes here....

--unique-boundary-2--

--unique-boundary-1
Content-type: text/richtext

This is <bold><italic>richtext.</italic></bold>
<smaller>as defined in RFC 1341</smaller>
<nl><nl>Isn't it
<bigger><bigger>cool?</bigger></bigger>

--unique-boundary-1
Content-Type: message/rfc822

From: (mailbox in US-ASCII)
To: (address in US-ASCII)
Subject: (subject in US-ASCII)
Content-Type: Text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: Quoted-printable

   ... Additional text in ISO-8859-1 goes here ...

--unique-boundary-1--

END
'(("\n" . "\r\n")))
)




(with-multipart-port simple-body "simple boundary" (lambda ()
  (test-group "parse simple mutltipart example"
    (test #t (char-ready?))
    (test "line-1" "" (read-line))
    (test "line-2" "This is implicitly typed plain ASCII text." (read-line))
    (test "line-3" "It does NOT end with a linebreak." (read-line))
    (test "line-4" "--simple boundary" (read-line))
    (test "line-5" "Content-type: text/plain; charset=us-ascii" (read-line))
    (test "line-6" "" (read-line))
    (test "line-7" "This is explicitly typed plain ASCII text." (read-line))
    (test "line-8" "It DOES end with a linebreak.\r\n" (read-string))
    (test "char-ready? looks ahead" #f (char-ready?))
    (test "multiple calls to char-ready? doesn't change answer"
          (map (lambda (_)#f) (iota 10)) (map (lambda (_) (char-ready?)) (iota 10)))
    (test "eof-1" #!eof (read-line))
    (test "eof-2" #!eof (read-line))
    (test "eof-3" #!eof (read-line))
    )))

(with-multipart-port complex-body "unique-boundary-1" (lambda ()
  (let ((lines (read-lines)))
    (test-group "parse complex multipart example"
      (test "line count" 51 (length lines))
      (test-assert "first line is blank" (string-null? (car lines)))
      (test "last line is correct"
            "   ... Additional text in ISO-8859-1 goes here ..."
            (last lines))
      ))))

(with-multipart-port "--b\r\nmime-part\r\n--b--\r\nepilogue" "b" (lambda ()
  (test "first delim doesn't require leading \r\n"
        "mime-part" (read-string))))
  


(test-group "malformed multipart"
  (with-multipart-port "" "b" (lambda ()
    (test "empty message" #!eof (read-line))))
  
  (with-multipart-port "preamble only\r\n--b\r\n" "b" (lambda ()
    (test "preamble only" #!eof (read-line))))
  
  (with-multipart-port "close-delim first \r\n--b--\r\nepilogue" "b" (lambda ()
    (test "close-delim first" #!eof (read-line))))
  
  (with-multipart-port "empty parts\r\n--b\r\n\r\n--b--\r\nepilogue" "b" (lambda ()
    (test "empty parts" #!eof (read-line))))

  (with-multipart-port "preamble\r\n--b\r\n\r\n\r\n\r\n--b--\r\nepilogue" "b" (lambda ()
    (test-group "2 empty lines in parts"
      (test "" (read-line))
      (test "" (read-line))))))


(test-group "mime-part-iterator"
  (wifs simple-body (lambda()
    (let ((next! (make-mime-part-iterator (curin) "simple boundary")))
      (let ((first (next!)))
        (test "1st part"
              "\r\nThis is implicitly typed plain ASCII text.\r\nIt does NOT end with a linebreak."
              (read-string #f first)))
      (let ((two (next!)))
        (test "2nd part"
              (conc "Content-type: text/plain; charset=us-ascii\r\n"
                    "\r\n"
                    "This is explicitly typed plain ASCII text.\r\n"
                    "It DOES end with a linebreak.\r\n")
              (read-string #f two))
        (test "no more parts" #f (next!))))))
  (test-group "mime-part-iterator-seek"
    ;;; When last mime-port isn't read completely, the rest of that part is
    ;;; skipped before the next mime-part port is given.
    (wifs simple-body (lambda ()
      (let ((next! (make-mime-part-iterator (curin) "simple boundary")))
        (let ((one (next!)))
          (read-char one))
        (let ((two (next!)))
          (test "seek to next mime-part"
                "Content-type: text/plain; charset=us-ascii"
                (read-line two)))
        (test "no more parts" #f (next!))
        (test "still no more parts" #f (next!)))))))





(test-exit)
